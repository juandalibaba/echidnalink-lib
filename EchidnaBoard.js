const five = require('johnny-five');
const { Notification } = require('electron')

const TEXTS = {
    ERROR: 'Error',
    PERFECT: 'Perfecto!',
    READY: 'Echidna está conectado y listo para ser controlado con Scratch!',
    NOT_CONNECTED: 'No encuentro al echidna, asegurate de que está conectado.'
}

const LEDS = {
    red: 'd13',
    yellow: 'd12',
    green: 'd11'
};

const RGB = {
    red: 'd9',
    green: 'd5',
    blue: 'd6'
};

const PIEZO = 'd10';

const DIGITAL_PINS = {
    d2: '2',
    d3: '3',
    d4: '4',
    d5: '5',
    d6: '6',
    d7: '7',
    d8: '8',
    d9: '9',
    d10: '10',
    d11: '11',
    d12: '12',
    d13: '13',
}

const ANALOG_PINS = {
    a0: '0',
    a1: '1',
    a2: '2',
    a3: '3',
    a4: '4',
    a5: '5',
    a6: '6',
    a7: '7'
}

function fail(e) {
    let m = (e.message == 'No connected device found') ?
        TEXTS.NOT_CONNECTED : e
    let n = new Notification({
        title: TEXTS.ERROR,
        body: m
    });
    if (this.tray != null) {
        this.tray.destroy();
    }

    this.server.close();
    n.show();
}

function ready() {
    // Aquí this es un objeto board
    console.log('Arduino is ready.');
    let n = new Notification({
        title: TEXTS.PERFECT,
        body: TEXTS.READY
    });
    n.show();
}

function resetEchidna(board) {
    if (!board.isReady) return;
    console.log("reseting echidna ...");
    for (let pin in DIGITAL_PINS) {
        board.digitalWrite(DIGITAL_PINS[pin], 0);
        board.analogWrite(DIGITAL_PINS[pin], 0);
    }
}

function configurePin(pinName, mode, board, io = null) {
    if (!board.isReady) return;

    if (mode == five.Pin.INPUT) {
        board.pinMode(DIGITAL_PINS[pinName], mode);
        board.digitalRead(DIGITAL_PINS[pinName], function (value) {
            io.emit('digitalInputValue', { id: pinName, value: value });
        })
    } else if (mode == five.Pin.OUTPUT) {
        board.pinMode(DIGITAL_PINS[pinName], mode);
    } else if (mode == five.Pin.ANALOG) {
        board.pinMode(ANALOG_PINS[pinName], mode);
        board.analogRead(ANALOG_PINS[pinName], function (value) {
            io.emit('analogInputValue', { id: pinName, value: value });
        })
    } else if (mode == five.Pin.PWM) {
        board.pinMode(DIGITAL_PINS[pinName], mode);
    }

}

function led(data, board) {
    if (!board.isReady) return;

    console.log(data)

    let pinName = LEDS[data.name];

    configurePin(pinName, five.Pin.OUTPUT, board);

    if (data.action == 'on') {
        board.digitalWrite(DIGITAL_PINS[pinName], 1);
    } else if (data.action == 'off') {
        board.digitalWrite(DIGITAL_PINS[pinName], 0);
    } else {
        console.log(data.action);
        console.log("LED: orden no reconocida");
    }
}

function rgb(data, board) {
    if (!board.isReady) return;

    console.log(data);

    for (let color in RGB) {
        let pinName = RGB[color];
        configurePin(pinName, five.Pin.PWM, board);
    }

    if (data.action == 'color') {
        board.analogWrite(DIGITAL_PINS[RGB["red"]], data.color["red"]);
        board.analogWrite(DIGITAL_PINS[RGB["green"]], data.color["green"]);
        board.analogWrite(DIGITAL_PINS[RGB["blue"]], data.color["blue"]);
    } else {
        console.log(data.action);
        console.log("RGB: orden no reconocida");
    }
}

function servo(data, board) {
    if (!board.isReady) return;

    // No sé lo que pasa, pero en el momento en que se crean los
    // servos, el pin 9 PWM (rgb rojo) deja de funcionar.
    // Así que en el momento en que se use el servo, el pin 9 se jode
    if (!board.servos) board.servos = {};
    board.servos[data['pin']] = new five.Servo(DIGITAL_PINS[data['pin']]);

    let pin = data['pin'];
    if (pin != 'd4' && pin != 'd7' && pin != 'd8') {
        console.log("pin no válido para servo")
        return;
    }
    let angle = data["angle"]
    board.servos[pin].to(angle);
}

function piezoOnOff(data, board) {
    console.log("PIEZO-ON/OFF");
    console.log(data);
    if (!board.isReady) return;

    configurePin(PIEZO, five.Pin.PWM, board);

    if (data['action'] == 'on') {
        board.analogWrite(DIGITAL_PINS[PIEZO], 100);
    }

    if (data['action'] == 'off') {
        board.analogWrite(DIGITAL_PINS[PIEZO], 0);
    }

}

function piezo(data, board) {

    console.log("PIEZO");
    console.log(data);
    if (!board.isReady) return;

    if (data['action'] == 'playsound') {
        configurePin(PIEZO, five.Pin.PWM, board);
        let intensity = 100;
        if ('intensity' in data) {
            intensity = parseFloat(data['intensity']);
        }
        console.log(`Intensity: ${intensity}`)
        board.analogWrite(DIGITAL_PINS[PIEZO], intensity);
        setTimeout(() => { board.analogWrite(DIGITAL_PINS[PIEZO], 0); }, data.duration)
    }


}

function playSong(data, board) {
    console.log("PLAYSONG");
    console.log(data);
    if (!board.isReady) return;

    let piezo = new five.Piezo(DIGITAL_PINS[PIEZO]);
    piezo.play({
        // song is composed by a string of notes
        // a default beat is set, and the default octave is used
        // any invalid note is read as "no note"
        song: data.melody,
        beats: 1 / 4,
        tempo: data.tempo
    })
}

function digitalOutput(data, board) {
    if (!board.isReady) return;

    console.log(data);

    configurePin(data.id, five.Pin.OUTPUT, board);

    board.digitalWrite(DIGITAL_PINS[data.id], data.value);
}

function digitalInput(data, board, io) {
    if (!board.isReady) return;

    console.log(data);

    configurePin(data.id, five.Pin.INPUT, board, io);

    /* board.pins[data.id].query(function(state) {
        io.emit('digitalInputValue', { id: data.id, value: state.value })
    }) */
}

function analogInput(data, board, io) {
    if (!board.isReady) return;

    console.log(data);
    configurePin(data.id, five.Pin.ANALOG, board, io);
}

function define_events(io, board) {
    return function (client) {
        // client es un socket
        console.log("Cliente conectado con id: " + client.id);

        client.on('join', function (handshake) {
            console.log(handshake);
        });

        client.on('client_disconnect', data => {

            resetEchidna(board);
            console.log("Cliente desconectado ..." + data.id);
        })

        client.on('reset', data => {
            console.log("Reset event fired");
            resetEchidna(board);
        });

        // Outputs
        client.on('led', data => {
            led(data, board);
        });

        client.on('rgb', data => {
            rgb(data, board);
        });

        client.on('servo', data => {
            servo(data, board);
        });

        client.on('piezo-on-off', data => {
            piezoOnOff(data, board);
        });

        client.on('piezo', data => {
            piezo(data, board);
        });

        client.on('piezo-analog', data => {
            piezo(data, board);
        });

        client.on('play-song', data => {
            playSong(data, board);
        });

        client.on('digitalOutput', data => {
            digitalOutput(data, board);
        })

        client.on('digitalInput', data => {
            digitalInput(data, board, io);
        })

        client.on('analogInput', data => {
            analogInput(data, board, io);
        })
    }
}

function EchidnaBoard(io, tray, server, comPort = null) {
    let options = { repl: false };
    if (comPort != null) {
        options.port = comPort;
    }

    console.log(options);

    this.tray = tray;
    this.server = server;
    this.board = new five.Board(options);
    this.board.on("fail", fail.bind(this));
    this.board.on("ready", ready.bind(this));

    this.board.on("close", () => {
        if(this.tray != null){
            this.tray.destroy();
        }
        
        this.server.close();
        io.close();
    });

    io.on("connection", define_events(io, this.board));
    io.on("disconnect", function () {
        console.log('Disconnection ...');
        console.log(client);
    });
}

exports.EchidnaBoard = { EchidnaBoard }