const http = require("http");
const { Server } = require("socket.io");
const pjson = require('./package.json');
const os = require('os');
const { SerialPort } = require('serialport');
const { EchidnaBoard } = require('./EchidnaBoard.js');

/**
 * En el caso de windows lista los puertos disponibles y elige el mayor.
 * En otro caso devuelve null, para que johnny five se encargue de elegirlo
 * @returns Promise(string | null)
 */
function selectSerialPort() {
    const platform = os.platform();
    console.log(`Plaform: ${platform}`);
    if (platform == 'win32') {
        return SerialPort.list().then(ports => {
            let maxComNumber = 0;
            ports.forEach(port => {
                console.log(port);
                const comNumber = parseInt(port.path.replace('COM', ''), 10);
                if (comNumber > maxComNumber) {
                    maxComNumber = comNumber;
                }
            });

            return `COM${maxComNumber}`;
        });
    } else {
        return Promise.resolve(null);
    }
}

function startLink(tray) {

    console.log("echidnalink-lib " + pjson.version);
    const httpServer = http.createServer();
    const io = new Server(httpServer, {
        cors: {
            origin: true,
            credentials: true
        }
    });

    return selectSerialPort().then(comPort => {

        let board = new EchidnaBoard.EchidnaBoard(io, tray, httpServer, comPort);

        const port = 12652;

        httpServer.listen(port);
        console.log(`Server listening on http://localhost:${port}`);

        return board;
    });

}

exports.echidnalink = { startLink }
