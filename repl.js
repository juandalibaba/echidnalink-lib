const five = require("johnny-five"),
board = new five.Board();

board.on("ready", function () {
    // Creates a piezo object and defines the pin to be used for the signal
    let piezo = new five.Piezo(10);
    let pin = 10
    
    board.pinMode(pin, five.Pin.PWM);

    // Injects the piezo into the repl
    board.repl.inject({
        play: function () {
            piezo.play({
                // song is composed by a string of notes
                // a default beat is set, and the default octave is used
                // any invalid note is read as "no note"
                song: "C D F D A - A A A A G G G G - - C D F D G - G G G G F F F F - -",
                beats: 1 / 4,
                tempo: 100
            })
        },
        on: function(level){
            board.analogWrite(pin, level);
        },

        off: function(level){
            board.analogWrite(pin, level);
        }
    });


});